// All exercises must inherit from GenericExercise
ExerciseProjection.prototype = new GenericExercise();
ExerciseProjection.prototype.constructor = ExerciseProjection;

// Constructor
function ExerciseProjection( name, number, callback, gl, shaderName )
{
    GenericExercise.call(this, name, number, callback); // mandatory ...

    // ui ...
    this.diff = 0;

    // Add a full scene ...
    this.cameraAt = new Vector(8,8,5);
    this.cameraTo = new Vector(0,0,2);
    this.scene = new Scene();
    this.scene.AddCamera(
	     new Camera( this.cameraAt, //eyePos ,
		               this.cameraTo, //centerPos ,
		               new Vector(0,0,1), //up ,
		               // width , height , fov , near , far
		               512, 512, 50.0, 0.01, 1000.0
		  )
    );

    // your shader comes here ...
    this.shader = [];

    // Objects are defined here ..
    // the axis ...
    var line_shader = new DefaultShader( gl ) ;
    this.scene.AddShader( line_shader );
    var axis = new Axis( "Axis1" , line_shader , new Vector( 0 , 0 , 0 ) , 100.0 ) ;
    this.objects = [ [axis], [axis], [axis], [axis], [axis] ];

    // geometry 0 : triangles
    // Add four triangles
    this.shader.push( new ProjectionTexture( gl, 0, 12 ) );
    this.scene.AddShader( this.shader[0] );

    for( var i=0; i<4; ++i) {
	var triangle = new Triangle( "triangle-"+i, this.shader[0] );
	// add a callback for animation
	triangle.phase = i*Math.PI/2.0;
	triangle.SetAnimate( function( ticks, object ) {
	    // simple rotation / Z
	    object.SetMatrix(
		new Matrix().Scale(10.0).RotateZ( object.phase + ticks/100.0 )
	    );
	} );
	// push to scene ...
	this.objects[0].push( triangle ) ;
    };

    // Box
    this.shader.push( new ProjectionTexture( gl, 0, 6.01 ) );
    this.scene.AddShader( this.shader[1] );

    var box = new Box( "Box", this.shader[this.shader.length-1], [1.0,1.0,1.0,1.0] );
    box.SetAnimate( function(tick, obj) {
	obj.SetMatrix( new Matrix()
		       .Scale(4.0)
		       .RotateZ(tick*Math.PI/100) );
    });
    this.objects[1].push( box );

    // Sphere
    this.shader.push( new ProjectionTexture( gl, 0, 18 ));
    var sphere = new Sphere( "Sphere", this.shader[this.shader.length-1], 32, 64, [1,1,1,1] );
    sphere.SetAnimate( function(t,o) {
	o.SetMatrix( new Matrix().Scale(8).RotateZ( t*2*Math.PI*0.0025 ) )
    } );
    this.objects[2].push( sphere );

    // Torus
    torus = new Torus( "Torus", this.shader[this.shader.length-1], 32, 64, new Vector(1.0, 0.2, 0.6), 1, 0.2 );
    torus.SetAnimate( function(t,o) {
	o.SetMatrix( new Matrix().Scale(8).RotateX( Math.PI*t*0.01).RotateZ( t*2*Math.PI*0.001 ) )
    } );
    this.objects[3].push( torus );

    // Bezier ...
    this.shader.push( new ProjectionTexture( gl, 0, 1 ));
    this.scene.AddShader( this.shader[this.shader.length-1] );
    var gumbo = new Gumbo( "gumbo1", this.shader[this.shader.length-1], 16, [ 1, 1, 0.2, 1.0 ] );
    gumbo.SetAnimate( function(tick, obj) {
	obj.SetMatrix( new Matrix()
		       .Scale(0.4)
		       .Translate(new Vector(-4,-2,0))
		       .RotateZ(tick*Math.PI/100)
		       .Translate(new Vector( 4, -5, -3 ))
		     );
    });
    this.objects[4].push( gumbo );

    this.shader.push( new ProjectionTexture( gl, 0, 0.1 ));
    this.scene.AddShader( this.shader[this.shader.length-1] );
    var teapot = new Teapot( "teapot1", this.shader[this.shader.length-1], 16, [ 1, 0.2, 1, 1.0 ] );
    teapot.SetAnimate( function(tick, obj) {
	obj.SetMatrix( new Matrix()
		       .Scale(0.04)
		       .RotateZ(tick*Math.PI/100)
		       .RotateY(tick*Math.PI/250)
		       .RotateZ(tick*Math.PI/1000)
		       .Translate(new Vector(-5,5,0))
		     );
    });
    this.objects[4].push( teapot );


    // add parameters to UI
    this.divHTML = document.createElement("div");
    this.divHTML.id = "exo-param-"+number;
    this.divHTML.style.display = 'none';

    var menu = document.getElementById("menu");
    menu.insertBefore(this.divHTML, document.getElementById("param-bottom"));

    // the parameters are button ...
    var exercise = this;
    this.buttonMode = this.createButton("mode0", "XY", function(button){
	exercise.SetShaderMode(0); if(!animate)update();
	if( exercise.buttonMode != null )
	    exercise.buttonMode.setAttribute('class' , "button" );
	exercise.buttonMode = button;
	button.setAttribute('class' , "button-selected" );
    });
    this.divHTML.appendChild( this.buttonMode );
    this.buttonMode.setAttribute('class', "button-selected");
    this.divHTML.appendChild(
	this.createButton("mode1", "YZ", function(button){
	    exercise.SetShaderMode(1); if(!animate)update();
	    if( exercise.buttonMode != null )
		exercise.buttonMode.setAttribute('class' , "button" );
	    exercise.buttonMode = button;
	    button.setAttribute('class' , "button-selected" );
	})
    );
    this.divHTML.appendChild(
	this.createButton("mode2", "ZX", function(button){
	    exercise.SetShaderMode(2); if(!animate)update();
	    if( exercise.buttonMode != null )
		exercise.buttonMode.setAttribute('class' , "button" );
	    exercise.buttonMode = button;
	    button.setAttribute('class' , "button-selected" );
	})
    );

    // transition
    this.divHTML.appendChild( this.createButton("trans", "---", function(button){} ) );

    // geometry
    this.buttonGeo = this.createButton("geo0", "Triangle", function(button){
	exercise.SetMode(0); if(!animate)update();
	if( exercise.buttonGeo != null )
	    exercise.buttonGeo.setAttribute('class' , "button" );
	exercise.buttonGeo = button;
	button.setAttribute('class' , "button-selected" );
    });
    this.divHTML.appendChild( this.buttonGeo );
    this.buttonGeo.setAttribute('class', "button-selected");
    this.divHTML.appendChild(
	this.createButton("geo1", "Box", function(button){
	    exercise.SetMode(1); if(!animate)update();
	    if( exercise.buttonGeo != null )
		exercise.buttonGeo.setAttribute('class' , "button" );
	    exercise.buttonGeo = button;
	    button.setAttribute('class' , "button-selected" );
	})
    );
    this.divHTML.appendChild(
	this.createButton("geo2", "Sphere", function(button){
	    exercise.SetMode(2); if(!animate)update();
	    if( exercise.buttonGeo != null )
		exercise.buttonGeo.setAttribute('class' , "button" );
	    exercise.buttonGeo = button;
	    button.setAttribute('class' , "button-selected" );
	})
    );
    this.divHTML.appendChild(
	this.createButton("geo3", "Torus", function(button){
	    exercise.SetMode(3); if(!animate)update();
	    if( exercise.buttonGeo != null )
		exercise.buttonGeo.setAttribute('class' , "button" );
	    exercise.buttonGeo = button;
	    button.setAttribute('class' , "button-selected" );
	})
    );
    this.divHTML.appendChild(
	this.createButton("geo4", "Bezier", function(button){
	    exercise.SetMode(4); if(!animate)update();
	    if( exercise.buttonGeo != null )
		exercise.buttonGeo.setAttribute('class' , "button" );
	    exercise.buttonGeo = button;
	    button.setAttribute('class' , "button-selected" );
	})
    );

    // fill the geometry
    this.SetMode( 0 ) ; // triangles;
};


ExerciseProjection.prototype.SetShaderMode = function(mode) {
    for(var i=0; i<this.shader.length; ++i)
	this.shader[i].SetMode(mode);
};


ExerciseProjection.prototype.SetMode = function(mode) {
    // suppress the old geometry ...
    this.scene.RemoveAllObjects();
    // add the new one ...
    for(var i=0; i<this.objects[mode].length; ++i) {
	this.objects[mode][i].Animate(0);
	this.scene.AddObject( this.objects[mode][i] );
    }
};



ExerciseProjection.prototype.onkeypress = function( event ) {
    if ( event.key == 'Up' || event.key == 'z' || event.key == 'Z' ) {
	exercises[exo].addTranslateXYZ( 2, 1.0/5.0 );
    } else if( event.key == 'Down' || event.key == 's' || event.key == 'S' ) {
	exercises[exo].addTranslateXYZ( 2, -1.0/5.0 );
    } else if( event.key == 'Left' || event.key == 'q' || event.key == 'Q' ) {
	exercises[exo].addTranslateXYZ( 1, -1.0/5.0 );
    } else if( event.key == 'Right' || event.key == 'd' || event.key == 'D' ) {
	exercises[exo].addTranslateXYZ( 1, 1.0/5.0 );
    } else if( event.key == 'a' || event.key == 'A'  ) {
	exercises[exo].addTranslateXYZ( 0, -1.0/5.0 );
    } else if( event.key == 'e' || event.key == 'E' ) {
	exercises[exo].addTranslateXYZ( 0, 1.0/5.0 );
    }
    else
	return false;
    return true;
};
ExerciseProjection.prototype.addTranslateXYZ = function( xyz, val ) {
    this.cameraAt.m[xyz] += val;
    this.cameraTo.m[xyz] += val;
    this.scene.GetActiveCamera().ComputeMatrices();
    // call the postRedisplay() function (in webgl.js)
    if( !animate ) update();
};

ExerciseProjection.prototype.Display = function(einfo)
{   // modify displayed information
    einfo.innerHTML  = this.diff + " ms";
};

ExerciseProjection.prototype.Show = function() {
    this.divHTML.style.display = 'block';
    if( this.button != null )
	this.button.setAttribute( 'class', "button-selected" );
};

ExerciseProjection.prototype.Hide = function() {
    this.divHTML.style.display = 'none';
};

ExerciseProjection.prototype.setDimension = function(width, height) {
    this.scene.setWidth( width );
    this.scene.setHeight( height );
};

ExerciseProjection.prototype.Animate = function() {
    this.scene.Animate();
};

// called once, to initialize the GL data
ExerciseProjection.prototype.Prepare = function( gl )
{
    this.scene.Prepare( gl );
    // prepare all scene arrays ...
    for( var list = 0; list<this.objects.length; ++list )
	for( var i = 0 ; i < this.objects[list].length ; ++i ) {
	    this.objects[list][ i ].Prepare( gl ) ;
	}
};


ExerciseProjection.prototype.Draw = function( gl )
{
    // chrono ON
    var start = new Date().getMilliseconds();

    // here the drawings ...
    gl.enable( gl.DEPTH_TEST );
    this.scene.Draw(gl);

    // chrono OFF
    this.diff = (new Date()).getMilliseconds() - start;
};
