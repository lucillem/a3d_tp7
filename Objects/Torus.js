// All objects must inherit from Generic Object 
Torus.prototype = Object.create( GenericObject.prototype );
Torus.prototype.constructor = Torus ;

/** 
* Create a torus
* @param name name of this instance
* @param shader WebGL shader
*/
function Torus( name , shader, nTheta, nPhi, color, bigRadius, smallRadius )
{
    // Call parent constructor (mandatory !)
    GenericObject.call( this , name , shader ) ;
    
    //this.color = (color !== undefined && color instanceof Object) ? color : [1,1,1,1];
    this.color = (color !== undefined && color instanceof Array) ? color : [1,1,1,1];
    this.bigRadius = bigRadius;
    this.smallRadius = smallRadius;
    this.nTheta = nTheta;
    this.nPhi = nPhi;
} ;


/**
* Creates a torus point
*/
Torus.prototype.CreatePoint = function( data, theta, phi )
{
    var myPhi = Math.PI*2*phi/this.nPhi;
    var myTheta = Math.PI*2*theta/this.nTheta;

    var x = Math.cos(myTheta)*(this.smallRadius*Math.cos(myPhi) + this.bigRadius);
	var y = Math.sin(myTheta)*(this.smallRadius*Math.cos(myPhi) + this.bigRadius);
    var z = this.smallRadius*Math.sin(myPhi);

    var nx = 2*x - (2*x*this.smallRadius)/(Math.sqrt(x*x + y*y)) ;
    var ny = 2*y - (2*y*this.smallRadius)/(Math.sqrt(x*x + y*y)) ;
    var nz = 2*z ;
    var normalVector = new Vector(nx, ny, nz).Normalize();
    nx = normalVector.m[0] ;
    ny = normalVector.m[1] ;
    nz = normalVector.m[2] ;

    var tx = -Math.cos(myTheta)*this.smallRadius*Math.sin(myPhi);
    var ty = -Math.sin(myTheta)*this.smallRadius*Math.sin(myPhi);
    var tz = this.smallRadius * Math.cos(myPhi);
    var tangentVector = new Vector(tx, ty, tz).Normalize();
    tx = tangentVector.m[0];
    ty = tangentVector.m[1];
    tz = tangentVector.m[2];

    var bx = -Math.sin(myTheta)*(this.smallRadius*Math.cos(myPhi) + this.bigRadius);
    var by = Math.cos(myTheta)*(this.smallRadius*Math.cos(myPhi) + this.bigRadius);
    var bz = 0;
    var biTangentVector = new Vector(bx, by, bz).Normalize();
    bx = biTangentVector.m[0];
    by = biTangentVector.m[1];
    bz = biTangentVector.m[2];

	var u = theta / this.nTheta;
    var v = phi / this.nPhi;
    // push data (it's ok, you should let this)
    //console.log("bThor");
    this.addAPoint( data, x, y, z ); 
    this.addAColor( data, this.color );
    this.addANormal( data, nx, ny, nz );
    this.addATangent( data, tx, ty, tz ); 
    this.addABitangent( data, bx, by, bz );

    this.addTextureCoordinates( data, u, v );
    //console.log("bThor");
};


/**
* Overload Prepare
*/
Torus.prototype.Prepare = function( gl )
{    
    // TODO: in the following, you retreive the Triangle.Prepare 
    //       implementation. Replace it by something allowing to 
    //       draw a Torus! Use the given color ;-)
    // Create vertex buffer 
    this.vbo = gl.createBuffer();
    this.vbo.numItems = (this.nPhi + 1) * (this.nTheta + 1);
    var data = [];
    for (var theta = 0; theta <= this.nTheta; theta++) {
        for (var phi = 0; phi <= this.nPhi; phi++) {
            this.CreatePoint(data, theta, phi);
        }
    }

    gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data), gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);


    // Create index buffer 
    this.ibo = gl.createBuffer();
    this.ibo.numItems = (this.nPhi + 1) * this.nTheta * 2;
    var data = [];
    var indice = 0;
    for (var theta = 0; theta < this.nTheta; theta++) {
        for (var phi = 0; phi <= this.nPhi; phi++) {
            data[indice++] = phi + ((this.nPhi + 1) * (theta + 1));
            data[indice++] = phi + ((this.nPhi + 1) * (theta + 0));
        }
    }

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibo);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(data), gl.STATIC_DRAW);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
} ;

/**
* Overload draw
*/
Torus.prototype.Draw = function( gl , scn )
{
    // Let's the shader prepare its attributes
    this.shader.setAttributes( this.vbo );
    
    // Let's render ! Again, use drawElements()!
    gl.bindBuffer( gl.ARRAY_BUFFER , this.vbo ) ;
    gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER , this.ibo ) ; 
    gl.drawElements( gl.TRIANGLE_STRIP , this.ibo.numItems, gl.UNSIGNED_SHORT, 0 ) ; 
}

