// All objects must inherit from Generic Object
Sphere.prototype = Object.create( GenericObject.prototype );
Sphere.prototype.constructor = Sphere ;


/**
* Create a Sphere given only its discretization parameters and its color.
* The sphere is made into its own coordinates system.
* Then, its center is (0,0,0) and its radius 1.
*/
function Sphere( name , shader , nTheta, nPhi, color )
{
    // Call parent constructor (mandatory !)
    GenericObject.call( this , name , shader ) ;
    // at least a prism
    if( nTheta < 3 ) nTheta = 3;
    if( nPhi < 4 ) nPhi = 4;
    // save the data
    this.nTheta = nTheta;
    this.nPhi   = nPhi;
    this.color  = color;
} ;

/**
* Creates a sphere point
*/
Sphere.prototype.CreatePoint = function( data, theta, phi )
{
    // Here, phi should vary between 0 to Math.PI, and theta between 0 to 2*Math.PI ...
    // TODO: fill this functionn giving good values to the following 12 variables
    var myPhi = Math.PI*phi/this.nPhi;
    var myTheta = Math.PI*2*theta/this.nTheta;

    var x = Math.sin(myPhi)*Math.cos(myTheta);
	  var y = Math.sin(myPhi)*Math.sin(myTheta);
    var z = Math.cos(myPhi);

    var nx = 2*x ;
    var ny = 2*y ;
    var nz = 2*z ;
    var normalVector = new Vector(nx, ny, nz).Normalize();
    nx = normalVector.m[0] ;
    ny = normalVector.m[1] ;
    nz = normalVector.m[2] ;

    var tx = Math.cos(myPhi);
    var ty = Math.cos(myPhi);
    var tz = - Math.sin(myPhi);
    var tangentVector = new Vector(tx, ty, tz).Normalize();
    tx = tangentVector.m[0];
    ty = tangentVector.m[1];
    tz = tangentVector.m[2];

    var bx = - Math.sin(myTheta);
    var by = Math.cos(myTheta);
    var bz = 0;
    var biTangentVector = new Vector(bx, by, bz).Normalize();
    bx = biTangentVector.m[0];
    by = biTangentVector.m[1];
    bz = biTangentVector.m[2];

	var u = theta / this.nTheta;
    var v = 1-(phi / this.nPhi);
    // push data (it's ok, you should let this)
    //console.log("bShpere");
    this.addAPoint( data, x, y, z );
    this.addAColor( data, this.color );
    this.addANormal( data, nx, ny, nz );
    this.addATangent( data, tx, ty, tz );
    this.addABitangent( data, bx, by, bz );
    this.addTextureCoordinates( data, u, v );
    //console.log("aShpere");
};

/**
* Overload Prepare
*/
Sphere.prototype.Prepare = function( gl )
{
    // Create vertex buffer
    this.vbo = gl.createBuffer();
    this.vbo.numItems = (this.nPhi+1) * (this.nTheta+1);
    var data = [];
    // here, we associate a square [0..1]^2 to the
    // parametric form of a sphere (double loop ;-)).
    for (var theta = 0; theta <= this.nTheta; theta++) {
        for (var phi = 0; phi <= this.nPhi; phi++) {
            this.CreatePoint(data, theta, phi);
        }
    }

    gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data), gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);


    // Create index buffer
    this.ibo = gl.createBuffer();
    this.ibo.numItems = (this.nPhi+1) * this.nTheta * 2;
    var data = [];
    // here, we associate a square [0..1]^2 to the
    // parametric form of a sphere (double loop ;-)).
    var indice = 0;
    for (var theta = 0; theta < this.nTheta; theta++) {
        for (var phi = 0; phi <= this.nPhi; phi++) {
            data[indice++] = phi + ((this.nPhi+1) * (theta+1));
            data[indice++] = phi + ((this.nPhi+1) * (theta+0));
        }
    }

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.ibo);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(data), gl.STATIC_DRAW);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

} ;

/**
* Overload draw
*/
Sphere.prototype.Draw = function( gl , scn )
{
    // Let's the shader prepare its attributes
    this.shader.setAttributes( this.vbo );
    // Let's render !
    // TODO: use an element buffer!
    gl.bindBuffer( gl.ARRAY_BUFFER , this.vbo ) ;
    gl.bindBuffer( gl.ELEMENT_ARRAY_BUFFER , this.ibo ) ;
    gl.drawElements( gl.TRIANGLE_STRIP , this.ibo.numItems, gl.UNSIGNED_SHORT, 0 ) ;
}
