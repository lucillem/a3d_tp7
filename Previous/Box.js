// All objects must inherit from Generic Object
Box.prototype = Object.create(GenericObject.prototype);
Box.prototype.constructor = Box;

/**
* Create a unit box with size 2
* @param name name of this instance
* @param shader WebGL shader
*/
function Box(name, shader, color) {
    // Call parent constructor (mandatory !)
    GenericObject.call(this, name, shader);
    color = (color !== undefined) && (color instanceof Array)
        ? color : [255, 255, 255, 255];
    color[0] /= 255;
    color[1] /= 255;
    color[2] /= 255;
    color[3] /= 255;
    this.color = color;
};

/**
* Overload Prepare
*/
Box.prototype.Prepare = function (gl) {
    // vertices
    var vertices = [
        //x   y   z
        [ 1, -1, -1 ],   //0
        [ 1,  1, -1 ],   //1
        [ 1,  1,  1 ],   //2
        [ 1, -1,  1 ],   //3
        [-1, -1, -1 ],   //4
        [-1,  1, -1 ],   //5
        [-1,  1,  1 ],   //6
        [-1, -1,  1 ],   //7
    ];
    var indices = [
        0, 1, 2, 0, 2, 3, //Front
        1, 5, 6, 1, 6, 2, //Right
        5, 4, 7, 5, 7, 6, //Back
        4, 0, 3, 4, 3, 7, //Left
        3, 2, 6, 3, 6, 7, //Top
        4, 5, 1, 4, 1, 0  //Botom
    ];
    var normals = [
        [1, 0, 0], //Front
        [0, 1, 0], //Right
        [0, -1, 0], //Back
        [-1, 0, 0], //Left
        [0, 0, 1],//Top
        [0, 0, -1],//Botom
    ];
    var tangents = [
        [0, 0, 1], //Front
        [0, 0, 1], //Right
        [0, 0, 1], //Back
        [0, 0, 1], //Left
        [1, 0, 0], //Top
        [1, 0, 0], //Botom
    ];
    var bitangents = [
        [0, -1, 0], //Front
        [1, 0, 0], //Right
        [0, 1, 0], //Back
        [-1, 0, 0], //Left
        [0, 1, 0], //Top
        [0, -1, 0], //Botom
    ];
    var texcoords = [
        0, 0,
        1, 0,
        1, 1,
        0, 0,
        1, 1,
        0, 1,
    ];

    // Create vertex buffer
    this.vbo = gl.createBuffer();
    this.vbo.numItems = 36;

    var data = [];
    for (var v = 0; v < this.vbo.numItems; ++v) {
        //console.log("bBox");
        // You MUST respect the following order ...
        this.addAPoint(data, vertices[indices[v]]);
        // or "this.addAPoint( data, vertices[v][0], vertices[v][1], vertices[v][2] );"
        this.addAColor(data, this.color);
        this.addANormal(data, normals[Math.floor(v/6)]);
        this.addATangent(data, tangents[Math.floor(v/6)]);
        this.addABitangent(data, bitangents[Math.floor(v/6)]);
        this.addTextureCoordinates(data, texcoords[(v%6) * 2], texcoords[(v%6) * 2 + 1]);
    }

    gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data), gl.STATIC_DRAW);

};

/**
* Overload draw
*/
Box.prototype.Draw = function (gl, scn) {
    // Let's the shader prepare its attributes
    this.shader.setAttributes(this.vbo);

    // Let's render !
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
    gl.drawArrays(gl.TRIANGLES, 0, this.vbo.numItems);
}
