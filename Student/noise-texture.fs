// -*- c++ -*-
precision mediump float;

const vec3 skyColor = vec3(0.3,0.2,0.9);
const vec3 cloudColor  = vec3(0.99,0.95,0.98);

uniform float uScale;
uniform vec3  uLight;

varying vec3 vNormal;
varying vec3 vPositionObject; // position in the object coordinates system
varying vec3 vPositionWorld; // position in the world coordinates system

// Functions that create a procedural noise ...
float hash(vec3 p) {
    p  = fract( p*0.3183099+.1 );
	p *= 17.0;
    return fract( p.x*p.y*p.z*(p.x+p.y+p.z) );
}
float noise( in vec3 x ) {
    vec3 p = floor(x);
    vec3 f = fract(x);
    f = f*f*(3.0-2.0*f);
    return mix(mix(mix( hash(p+vec3(0,0,0)),
                        hash(p+vec3(1,0,0)),f.x),
                   mix( hash(p+vec3(0,1,0)),
                        hash(p+vec3(1,1,0)),f.x),f.y),
               mix(mix( hash(p+vec3(0,0,1)),
                        hash(p+vec3(1,0,1)),f.x),
                   mix( hash(p+vec3(0,1,1)),
                        hash(p+vec3(1,1,1)),f.x),f.y),f.z);
}
// end noise

/*Create a new procedural shader that produces a noisy result, like for marble
or cloud. The key idea is to use a “noise” function that produces a number in
[0 . . . 1] depending on a 3d position. You should add some call to this function
with scaled values of the fragment position to produce a noise factor. Then,
just mix two colors (eg. white and blue) to obtain final color.*/


float cloud( in vec3 x ) {
	// TODO: replace next line by something better ...
	return noise(normalize(x)*vPositionWorld);
}


// you do not need to modify the main function
void main(void)
{
	vec3 color = mix(skyColor, cloudColor, cloud(vPositionObject*uScale));
	float attenuation = max( 0.4, abs( dot( normalize(uLight-vPositionWorld), normalize(vNormal)) ) );
	gl_FragColor = vec4(attenuation * color, 1.0);
}
