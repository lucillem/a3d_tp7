// All shaders must inherit from Shader object 
BumpTexture.prototype = new Shader();
BumpTexture.prototype.constructor = BumpTexture ;

/* constructor */
function BumpTexture( gl, scale, light ) 
{
    Shader.call( this , "bump-texture",  
		 "./Student/bump-texture.vs", 
		 "./Student/bump-texture.fs", 
		 gl,
		 BumpTexture.prototype.attributes ) ;

    this.scale    = scale;
    this.light    = light;
} ;

BumpTexture.prototype.attributes = [
    AttributeEnum.position, AttributeEnum.color, AttributeEnum.normal, AttributeEnum.texcoord
];

BumpTexture.prototype.setAttributes = function ( vbo ) 
{
    gl.bindBuffer( gl.ARRAY_BUFFER , vbo ) ;    
    
    var attr_pos = this.GetAttributeLocation( "aPosition" ) ; 
    if( attr_pos>-1 ) {
	gl.enableVertexAttribArray( attr_pos ) ; 
	gl.vertexAttribPointer( attr_pos , 3 , gl.FLOAT , false , 48 ,  0 ) ; 
    }
    var attr_col = this.GetAttributeLocation( "aColor" ) ; 
    if( attr_col>-1 ) {
	gl.enableVertexAttribArray( attr_col ) ; 
	gl.vertexAttribPointer( attr_col , 4 , gl.FLOAT , false , 48 , 12 );
    }
    var attr_nor = this.GetAttributeLocation( "aNormal" ) ; 
    if( attr_nor>-1 ) {
	gl.enableVertexAttribArray( attr_nor ) ; 
	gl.vertexAttribPointer( attr_nor , 3 , gl.FLOAT , false , 48 , 28 );
    }
    var attr_tex = this.GetAttributeLocation( "aTexcoords" ) ; 
    if( attr_tex>-1 ) {
	gl.enableVertexAttribArray( attr_tex ) ; 
	gl.vertexAttribPointer( attr_tex , 2 , gl.FLOAT , false , 48 , 40 );
    }
    gl.uniform1f( this.GetUniformLocation( "uScale" ), this.scale )
    gl.uniform3fv( this.GetUniformLocation( "uLight" ), this.light.m );
} ;


