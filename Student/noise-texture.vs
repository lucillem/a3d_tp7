// -*- c++ -*- 
attribute vec3 aPosition ;
attribute vec3 aNormal ;

uniform mat4 uProjectionMatrix ;
uniform mat4 uModelViewMatrix ;
uniform mat4 uNormalMatrix ;

varying vec3 vNormal;
varying vec3 vPositionObject; // position in the object coordinates system
varying vec3 vPositionWorld; // position in the world coordinates system

void main( void )
{
	vPositionObject = aPosition;
	vPositionWorld = ( uModelViewMatrix * vec4( aPosition, 1.0 ) ) .xyz ;
	gl_Position = uProjectionMatrix * vec4( vPositionWorld, 1.0 );
	vNormal = (uNormalMatrix * vec4(aNormal,0)).xyz;
}
