// -*- c++ -*-
attribute vec3 aPosition ;
attribute vec4 aColor ;
attribute vec3 aNormal ;
attribute vec2 aTexcoords ;

uniform mat4 uProjectionMatrix ;
uniform mat4 uModelViewMatrix ;
uniform mat4 uNormalMatrix ;

varying vec4 vColor;
varying vec3 vNormal;
varying vec3 vPosition;
varying vec2 vTexcoords;

void main( void )
{
  gl_Position = uProjectionMatrix *  uModelViewMatrix * vec4( aPosition , 1.0 );
  vColor = aColor;
  vNormal = aNormal;
  vPosition = aPosition;
  vTexcoords = aTexcoords;
}
