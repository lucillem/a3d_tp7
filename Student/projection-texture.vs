// -*- c++ -*-
attribute vec3 aPosition ;

uniform mat4 uProjectionMatrix ;
uniform mat4 uModelViewMatrix ;

uniform int uMode ; // X Y Z ?

varying vec2 vTexCoord;

void main( void )
{
  gl_Position = uProjectionMatrix *  uModelViewMatrix * vec4( aPosition , 1.0 );

  // TODO: add what you need ..

  //Si uMode = 0 alors xTexCoord = aPosition.yz
  //sinon si mode== 1    alors xTexCoord = aPosition.zx
  //sinon si mode== 2   alors xTexCoord = aPosition.xy
  /*switch(mode){
    case 1 : vTexCoord = aPosition.yz;
    break;
    case 2 : vTexCoord = aPosition.zx;
    break;
    case 3 : vTexCoord = aPosition.xy;
    break;
  }*/




  vTexCoord = uMode == 0 ? aPosition.xy
    : uMode == 1 ? aPosition.yz
    : aPosition.zx;

}
