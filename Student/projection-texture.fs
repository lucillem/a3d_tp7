// -*- c++ -*-
precision mediump float;

uniform float uScale;

varying vec2 vTexCoord;

void main(void)
{

  vec2 uv = vTexCoord * uScale;
  uv = floor(uv);
  float f = mod(uv.x + uv.y, 6.0);

  if (f == 0.0){
    gl_FragColor = vec4(1,0,0,1);
  }else if (f == 1.0){
    gl_FragColor = vec4(1,1,0,1);
  }else if (f == 2.0){
    gl_FragColor = vec4(0,1,0,1);
  }else if (f == 3.0){
    gl_FragColor = vec4(0,1,1,1);
  }else if (f == 4.0){
    gl_FragColor = vec4(0,0,1,1);
  }else if (f == 5.0){
    gl_FragColor = vec4(1,0,1,1);
  }

}
