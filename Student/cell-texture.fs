// -*- c++ -*-
precision mediump float;

uniform float uScale;
uniform vec3  uLight;

varying vec4 vColor;
varying vec3 vNormal;
varying vec3 vPosition;
varying vec2 vTexcoords;

void main(void)
{

  /*Implement the toon shading. A fast solution consists to multiply the cosine
between the normal and the light direction by a given constant c, and then
to take its integer part as a lookup value in some shading colors. For the
black outline, you can ignore it or simulate such a behavior using a very small
absolute value of the cosine between the normal and the eye direction.*/
  float res = Math.cos(vNormal)*2.0;
  //gl_FragColor = vec4(vColor.x, res, res, res);
  gl_FragColor = vColor;

}
