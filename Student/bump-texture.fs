// -*- c++ -*-
precision mediump float;

uniform float uScale;
uniform vec3  uLight;

varying vec4 vColor;
varying vec3 vNormal;
varying vec3 vPosition;
varying vec2 vTexcoords;

void main(void)
{
    /*Create a new shader that modifies the magnitude of the normal at a given
  fragment, using a nice formula (choose one!). Then, as in previous exercise,
  use a dot product between the normalized direction from the fragment to the
  light source with the modified normal ...*/
  /* Créer un nouveau shader qui modifie la magnitude de la normal d'un fragment
  donné, en utilisant une belle formule (a choisir). Puis, comme dans l'exercice
  précédent, utiliser un dot product entre la norme de direction du fragment à la
  souce de lumière avec la normal modifiée */
  //normal = (2*color)-1
  vec3 modifyNormal = (2.0*vColor.xyz)-vec3(1.0,1.0,1.0);
  float c = dot(normalize(vNormal), normalize(modifyNormal));
  gl_FragColor = vec4(c,1.0,1.0,1.0);
}
