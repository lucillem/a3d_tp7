// -*- c++ -*-
precision mediump float;

//varying vec3 aPosition ;
//varying vec3 aNormal ;

uniform sampler2D uTexture0; // jour
uniform sampler2D uTexture1; //nuit

uniform float uScale; // ??
uniform vec3  uLight;    //position de la lumière

varying vec2 vTexcoords;
varying vec3 vPosition;
varying vec3 vNormal;
void main(void)
{
  //vec3 normalLight = normalize(uLight)-vPosition.xyz;

  float c = dot(normalize(uLight), normalize(vPosition));
  gl_FragColor = mix(texture2D(uTexture0, vTexcoords),
              texture2D(uTexture1, vTexcoords),
              smoothstep(-0.05, +0.05, c));
}
