// All shaders must inherit from Shader object 
NoiseTexture.prototype = new Shader();
NoiseTexture.prototype.constructor = NoiseTexture ;

/* constructor */
function NoiseTexture( gl, scale, light ) 
{
    Shader.call( this , "noise-texture",  
		 "./Student/noise-texture.vs", 
		 "./Student/noise-texture.fs", 
		 gl,
		 NoiseTexture.prototype.attributes ) ;

    this.scale    = scale;
    this.light    = light;
} ;

NoiseTexture.prototype.attributes = [
    AttributeEnum.position, AttributeEnum.normal
];

NoiseTexture.prototype.setAttributes = function ( vbo ) 
{
    gl.bindBuffer( gl.ARRAY_BUFFER , vbo ) ;    
    
    var attr_pos = this.GetAttributeLocation( "aPosition" ) ; 
    gl.enableVertexAttribArray( attr_pos ) ; 
    
    var attr_nor = this.GetAttributeLocation( "aNormal" ) ; 
    gl.enableVertexAttribArray( attr_nor ) ; 

    // Fill all parameters for rendering 
    gl.vertexAttribPointer( attr_pos , 3 , gl.FLOAT , false , 24 ,  0 ) ; 
    gl.vertexAttribPointer( attr_nor , 3 , gl.FLOAT , false , 24 , 12 );

 
    gl.uniform1f( this.GetUniformLocation( "uScale" ), this.scale )
    gl.uniform3fv( this.GetUniformLocation( "uLight" ), this.light.m );
} ;


