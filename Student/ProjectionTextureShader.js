// All shaders must inherit from Shader object 
ProjectionTexture.prototype = new Shader();
ProjectionTexture.prototype.constructor = ProjectionTexture ;

/* constructor */
function ProjectionTexture( gl, mode, scale ) {
    Shader.call( this , "projection",  
		 "./Student/projection-texture.vs", 
		 "./Student/projection-texture.fs", 
		 gl,
		 ProjectionTexture.prototype.attributes ) ;

    // projection mode: 0 to 2 (0 for X, 1 for Y and 2 for Z)
    this.mode = ( mode<0 ) ? 0 : (mode>2) ? 2 : mode;
    this.scale = scale;
} ;

ProjectionTexture.prototype.SetMode = function(mode) {
    if (mode >= 0 && mode <= 2)
	this.mode = mode;
};

ProjectionTexture.prototype.attributes = [
    AttributeEnum.position
];

ProjectionTexture.prototype.setAttributes = function ( vbo ) 
{
    gl.bindBuffer( gl.ARRAY_BUFFER , vbo ) ;    
    
    // Get Position attribute
    var attr_pos = this.GetAttributeLocation( "aPosition" ) ; 
    
    // Activate Attribute 
    gl.enableVertexAttribArray( attr_pos ) ; 
    
    // Fill all parameters for rendering 
    gl.vertexAttribPointer( attr_pos , 3 , gl.FLOAT , false , 12 , 0 ) ; 
    
    gl.uniform1i( this.GetUniformLocation( "uMode" ), this.mode );    
    gl.uniform1f( this.GetUniformLocation( "uScale" ), this.scale );
} ;


