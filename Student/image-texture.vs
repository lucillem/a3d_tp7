// -*- c++ -*-
attribute vec3 aPosition ;
attribute vec3 aNormal ;
attribute vec2 aTexcoords ;

uniform mat4 uProjectionMatrix ;
uniform mat4 uModelViewMatrix ;
uniform mat4 uNormalMatrix ;

varying vec2 vTexcoords;

varying vec3 vPosition;

varying vec3 vNormal;
void main( void )
{

  gl_Position = uProjectionMatrix *  uModelViewMatrix * vec4( aPosition , 1.0 );
  vTexcoords = aTexcoords;
  vPosition = aPosition;
  vNormal = aNormal;
}
