// All shaders must inherit from Shader object
ImageTexture.prototype = new Shader();
ImageTexture.prototype.constructor = ImageTexture ;

/* constructor */
function ImageTexture( gl, scale, tex0, tex1, light )
{
    Shader.call( this , "image-texture",
		 "./Student/image-texture.vs",
		 "./Student/image-texture.fs",
		 gl,
		 ImageTexture.prototype.attributes ) ;

    this.scale    = scale;
    this.texture0 = tex0;
    this.texture1 = tex1;
    this.light    = light;
} ;

ImageTexture.prototype.attributes = [
    AttributeEnum.position, AttributeEnum.normal, AttributeEnum.texcoord
];

ImageTexture.prototype.setAttributes = function ( vbo )
{
    gl.bindBuffer( gl.ARRAY_BUFFER , vbo ) ;

    var attr_pos = this.GetAttributeLocation( "aPosition" ) ;
    if( attr_pos > -1 ) {
	gl.enableVertexAttribArray( attr_pos ) ;
	gl.vertexAttribPointer( attr_pos , 3 , gl.FLOAT , false , 32 ,  0 ) ;
    }
    var attr_nor = this.GetAttributeLocation( "aNormal" ) ;
    if( attr_nor>-1 ){
	gl.enableVertexAttribArray( attr_nor ) ;
	gl.vertexAttribPointer( attr_nor , 3 , gl.FLOAT , false , 32 , 12 );
    }
    var attr_tex = this.GetAttributeLocation( "aTexcoords" ) ;
    if( attr_tex>-1) {
	gl.enableVertexAttribArray( attr_tex ) ;
	gl.vertexAttribPointer( attr_tex , 2 , gl.FLOAT , false , 32 , 24 );
    }
    // Fill all parameters for rendering

    this.texture0.bind(gl, this.GetUniformLocation( "uTexture0" ));
    this.texture1.bind(gl, this.GetUniformLocation( "uTexture1" ));

    gl.uniform1f( this.GetUniformLocation( "uScale" ), this.scale )

    gl.uniform3fv( this.GetUniformLocation( "uLight" ), this.light.m );

} ;
