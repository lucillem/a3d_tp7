// Texture's constructor
function Texture( gl, file, id ) 
{
    this.id = (id === undefined) ? 7 : id;
    this.texture = null;
    if( gl != null ) 
	this.set( gl, file );
};

// binder
Texture.prototype.bind = function( gl, location ) {
    if( this.texture.loaded == 1 ) {
	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	gl.activeTexture(gl.TEXTURE0+this.id);
	gl.bindTexture(gl.TEXTURE_2D, this.texture );
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.texture.image);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT); //CLAMP_TO_EDGE);
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT); //CLAMP_TO_EDGE);	
	gl.bindTexture(gl.TEXTURE_2D, null); // unbind
	this.texture.loaded = 2;
    }
    if( this.texture.loaded == 2 ) 
    {
	/*switch(this.id) {
	case 0: gl.activeTexture(gl.TEXTURE0); break;
	case 1: gl.activeTexture(gl.TEXTURE1); break;
	case 2: gl.activeTexture(gl.TEXTURE2); break;
	case 3: gl.activeTexture(gl.TEXTURE3); break;
	case 4: gl.activeTexture(gl.TEXTURE4); break;
	case 5: gl.activeTexture(gl.TEXTURE5); break;
	case 6: gl.activeTexture(gl.TEXTURE6); break;
	case 7: gl.activeTexture(gl.TEXTURE7); break;
	};*/
	gl.activeTexture(gl.TEXTURE0+this.id);
	gl.bindTexture(gl.TEXTURE_2D, this.texture);
	gl.uniform1i(location, this.id);
    }
};

Texture.prototype.unbind = function() {
    if( this.texture.loaded == 2 ) 
	gl.bindTexture(gl.TEXTURE_2D, null);
};

Texture.prototype.set = function( gl, textureName ) {
    this.reset();
    this.texture = gl.createTexture();
    this.texture.image = null;
    this.texture.loaded = 0;
    this.texture.image = new Image();
    this.texture.image.onloadend = (function (texture) {texture.loaded=1;})(this.texture);
    this.texture.image.src = textureName;
};


Texture.prototype.reset = function() {
    if (this.texture != null)
	gl.deleteTexture( this.texture );
};
